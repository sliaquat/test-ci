#!/usr/bin/env ruby

class GenerateQAJobs
  def execute
    <<~YML
      image: ruby:latest

      stages:
        - test 

      job1:
        stage: test
        needs:
          - pipeline: $PARENT_PIPELINE_ID
            job: generate-jobs
            artifacts: true
        script:
          - echo $SHL_VAR

      job2:
        stage: test        
        needs:
          - pipeline: $PARENT_PIPELINE_ID
            job: generate-jobs
            artifacts: true
        extends:
          - .rules:shl
        script:
          - echo $SHL_VAR

    YML
  end

end

jobs = GenerateQAJobs.new.execute

File.open('generated-qa-jobs.yml', 'w') { |f| f.write(jobs) }
